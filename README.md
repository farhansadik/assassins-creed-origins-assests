# Assassins Creed Origins Assets 

![wallpaper](images/ac_origins_wallpaper.jpg)

**Download Link**
> `Codex` Fitgirl Repack 

[https://www.1377x.to/torrent/3310762/Assassin-s-Creed-Origins-v1-5-1-All-DLCs-Crackfix-MULTI15-FitGirl-Repack-Selective-Download-from-28-1-GB/](https://www.1377x.to/torrent/3310762/Assassin-s-Creed-Origins-v1-5-1-All-DLCs-Crackfix-MULTI15-FitGirl-Repack-Selective-Download-from-28-1-GB/)

**Game Save**

> `Codex` Crack

**File Location**

> %SystemDrive%\Users\Public\Documents\uPlay\CODEX\Saves\AssassinsCreedOrigins

## Game Settings
Based on my system configuration
```
  CPU       : AMD Ryzen 3 3100 
  RAM       : 8GB DDR4-3400MHz
  SSD       : Walton Antique 256GB nvme m.2 SSD 
  HDD       : ATA TOSHIBA DT01ACA0 500GB
  Mobo      : B450M
  GPU       : Sapphire Pulse RX 580 8GB
  OS        : Windows 10 Pro 22H2 
  Display   : LG 1080p
```

**Display Settings**

| Brightness          | 60%     |
|---------------------|---------|
| Resolution Modifier | 100%    |
| Vsync               | ON      |
| Field of View       | 100/115 |

**Graphics Settings**

| General                 |            |
|-------------------------|------------|
| Dynamic Resolution      | Off        |
| Anti-Aliasing           | High       |
| Shadows                 | High       |
| Environment Details     | High       |
| Texture Detail          | High       |
| Tessellation            | High       |
| Terrain                 | High       |
| Clutter                 | High       |
| Fog                     | High       |
| Water                   | High       |
| Screen Space Reflection | High       |
| Volumetric Clouds       | ON         |
| Characters              |            |
| Texture Detail          | High       |
| Character               | Ultra High |
| Post Processing         |            |
| Ambient Occlusion       | High       |
| Depth of Field          | ON         |

> **AMD Adrenaline Settings Against VSync** <br> 
> Radeon Enhanced Sync - Disabled <br>
> Radeon Anti-Lag - Enabled <br>
> AMD FreeSync - ON


## Benchmark Result
![](images/benchmark_results.jpg)

## Cheat Engine Table
Download Cheat Engine :  <br>
[![cheat_engine](images/cheat_engine_logo.png)](https://www.cheatengine.org/downloads.php)

**Understanding Cheat Table:**

 * enable .2
	
	* undead .5
	> You'll never die while playing....

	* ignore adrenaline .4
	> This will ignore gathering adrenaline, auto generate to full adrenaline while combat.
	
	* instant charge heavy attack 
	> This will ignore gathering adrenaline, auto generate to full adrenaline while combat.
	
	* instant bow charge .2
	> This will ignore gathering adrenaline, auto generate to full adrenaline while combat.
	
	* ignore $
	> Ignore Money, you can buy anything from clothes, stable and blacksmith. ***But Only you can do when you've no money***
	
	* ignore ability points .2
	> No ability will be required to update skill. You can unlock any skill. 
	
	* ignore resources 
	> No resources will be required to update ######. You can unlock any ######. 

	* exp gained multiplier
	> You'll get extra more XP, while completing any quest

	* slow motion on bow aim
	> No thing to mansion! 
	
 * weapon editor .3

    > Never tested!! 


**Source :**

* Keymap - [Ubisoft Support](https://support.ubisoft.com/en-GB/faqs/000031689/PC-Controls-for-Assassin-s-Creed-Origins-ACO/)

